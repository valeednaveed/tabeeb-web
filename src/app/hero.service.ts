import {Injectable} from '@angular/core';
import {Hero} from './Hero';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  url = 'http://tabeeb-app.herokuapp.com/api/hospital';
  constructor(private http: HttpClient) { }


  getHeroes(): Observable<Hero[]> { // URL WILL BE PLACED HERE
    // return of(HEROES);
    return this.http.get<Hero[]>(this.url);

  }
}

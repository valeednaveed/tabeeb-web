import { Hero } from './Hero';

export const HEROES: Hero[] = [
  { uuid: '11', name: 'Mr. Nice', address: 'abc' },
  { uuid: '12', name: 'Narco', address: 'abc' }
  // { uuid: 13, name: 'Bombasto' },
  // { uuid: 14, name: 'Celeritas' },
  // { uuid: 15, name: 'Magneta' },
  // { uuid: 16, name: 'RubberMan' },
  // { uuid: 17, name: 'Dynama' },
  // { uuid: 18, name: 'Dr IQ' },
  // { uuid: 19, name: 'Magma' },
  // { uuid: 20, name: 'Tornado' }
];
